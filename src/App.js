import React, { useState, useEffect } from "react";
import MyModal from "./pages/Signup";
import { Routes, Route } from "react-router-dom";
import Login from './pages/Login';

function App() {
  return (
    <div className="absolute p-0 w-full h-full bg-green-400">
      <Login />
      {/* <MyModal /> */}
      {/* <Routes>
        <Route exact path="/" element={<Login />} />
      </Routes> */}
    </div>
  );
}

export default App;
